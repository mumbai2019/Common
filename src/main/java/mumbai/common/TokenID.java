package mumbai.common;

/**
 * Based on the structure showcased in the slides.
 * @author s164166 Patrick
 *
 */
public interface TokenID {
	public String getID();
}
