package mumbai.common.restClient;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import mumbai.common.tools.Path;

/**
 * Common Rest client to perform REST requests
 * @author s172596 Diego
 *
 */
public class RestClient 
{
	private static Client client = ClientBuilder.newClient();
	private Path rootPath;

	public RestClient(String rootPath) {
		this.rootPath = new Path(rootPath);
	}	
	
	/**
	 * 
	 * @param path
	 * @param mediaType
	 * @return
	 */
	public Response get(String path, String mediaType)
	{
		System.out.println("Address: " + this.rootPath.addPath(path));
		WebTarget webTarget = client.target( this.rootPath.addPath(path) );
        Invocation.Builder invocationBuilder = webTarget.request(mediaType);
        Response response = invocationBuilder.get();
        response.bufferEntity();
        return response;
	}
	
	/**
	 * 
	 * @param path
	 * @param content
	 * @param mediaType
	 * @return
	 */
	public Response post(String path, String content, String mediaType)
	{
		WebTarget webTarget = client.target( this.rootPath.addPath(path) );
        Invocation.Builder invocationBuilder = webTarget.request(mediaType);
        Response response = invocationBuilder.post(Entity.entity(content, mediaType));
        response.bufferEntity();
        return response;
	}
	
	/**
	 * 
	 * @param path
	 * @param content
	 * @param mediaType
	 * @return
	 */
	public Response put(String path, String content, String mediaType)
	{
		WebTarget webTarget = client.target( this.rootPath.addPath(path) );
        Invocation.Builder invocationBuilder = webTarget.request(mediaType);
        Response response = invocationBuilder.put(Entity.entity(content, mediaType));
        response.bufferEntity();
        return response;
	}
	
	/**
	 * 
	 * @param path
	 * @param mediaType
	 * @return
	 */
	public Response delete(String path, String mediaType)
	{
		WebTarget webTarget = client.target( this.rootPath.addPath(path) );
        Invocation.Builder invocationBuilder = webTarget.request(mediaType);
        Response response = invocationBuilder.delete();
        response.bufferEntity();
        return response;
	}
}
