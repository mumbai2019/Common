package mumbai.common;

/**
 * User value object
 * @author s172596 Diego
 *
 */
public interface UserID {

	public String getID();
}
