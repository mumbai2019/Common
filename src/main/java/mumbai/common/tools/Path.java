package mumbai.common.tools;

public class Path 
{
	private String path;
	
	public Path(String path) {
		if( path.lastIndexOf('/') != path.length()-1 )
			this.path = path + "/";
		else
			this.path = path;
	}
	
	public String getPath() {
		return this.path;
	}
	
	public String addPath(String path) {
		String tempString = this.path;
		if( path.indexOf('/') == 0 ) 
			tempString += path.substring( 1, path.length());
		else
			tempString += path;
		
		return tempString;
	}
}
