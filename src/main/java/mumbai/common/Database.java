package mumbai.common;

import java.util.Optional;


public interface Database {
	
	public void addElement(Object element);
	public boolean elementExists(Object elementID);
	public Object getElement(Object elementID);

}
