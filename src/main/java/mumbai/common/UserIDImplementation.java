package mumbai.common;

public class UserIDImplementation implements UserID{

	private String id;
	/**
	 * Set an UUID to the id value
	 * @param id
	 * @author s164166 Patrick
	 */
	public UserIDImplementation(String id)
	{
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 97;
		int result = 3;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserIDImplementation other = (UserIDImplementation) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	/**
	 * Returns the ID number of the customer
	 * @author s164166 Patrick
	 */
	@Override
	public String getID() {
		return id;
	}
}
